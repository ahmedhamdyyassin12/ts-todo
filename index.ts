import axios from "axios";

const url = "https://jsonplaceholder.typicode.com/todos/1";
axios.get(url).then(response => {
    
    interface Todo {
        id: number,
        title: string,
        completed: boolean
    }
    
    const todo = response.data as Todo;

    const id = todo.id;
    const Title = todo.title;
    const Completed = todo.completed;

    console.log(`
    The Todo with ID ${id} 
    Has a tilte of: ${Title}
    Is it finished: ${Completed}
    `);
    
})